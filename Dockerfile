FROM python:3.9-slim-buster
EXPOSE 5000

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

# Install poetry
RUN pip install --disable-pip-version-check --no-cache-dir poetry

# Install minifiers
RUN pip install --disable-pip-version-check --no-cache-dir rjsmin rcssmin

COPY . /app
WORKDIR /app/donate_app/static

ARG PRODUCTION=true

RUN if [ "$PRODUCTION" = "true" ] ; then \
    python -mrjsmin < donate.js > donate-min.js && \
    python -mrcssmin < site.css > site-min.css && \
    python -mrcssmin < style.css > style-min.css && \
    python -mrcssmin < normalize.css > normalize-min.css; \
    else \
    cp donate.js donate-min.js && \
    cp site.css site-min.css && \
    cp style.css style-min.css && \
    cp normalize.css normalize-min.css; \
    fi

WORKDIR /app

RUN poetry config virtualenvs.create false && poetry install --no-root --no-dev

RUN useradd appuser && chown -R appuser /app
USER appuser

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "startup:app"]
